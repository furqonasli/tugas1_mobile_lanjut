import {Text, View, TextInput, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      fullName: '',
    };
  }

  render() {
    return (
      <View>
        <Text
          style={{
            fontSize: 20,
            fontWeight: 'bold',
            marginBottom: 10,
            marginLeft: 10,
          }}>
          First Name:
        </Text>
        <TextInput
          style={{
            margin: 10,
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 10,
            paddingRight: 10,
          }}
          placeholder="masukkan nama depan"
          onChangeText={text => this.setState({firstName: text})}
        />
        <Text
          style={{
            fontSize: 20,
            fontWeight: 'bold',
            marginBottom: 10,
            marginLeft: 10,
          }}>
          Last Name:
        </Text>
        <TextInput
          style={{
            margin: 10,
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
            borderRadius: 5,
            paddingLeft: 10,
            paddingRight: 10,
          }}
          placeholder="masukkan nama belakang"
          onChangeText={text => this.setState({lastName: text})}
        />
        <TouchableOpacity
          style={{
            backgroundColor: '#ff0000',
            padding: 10,
            margin: 10,
            borderRadius: 5,
            alignItems: 'center',
          }}
          onPress={() =>
            this.setState({
              fullName: this.state.firstName + ' ' + this.state.lastName,
            })
          }>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: '#fff',
            }}>
            Show Full Name
          </Text>
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 20,
            fontWeight: 'bold',
            margin: 10,
          }}>
          {this.state.fullName}
        </Text>
      </View>
    );
  }
}

export default App;
